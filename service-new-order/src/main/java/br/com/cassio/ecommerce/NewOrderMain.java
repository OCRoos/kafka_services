package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.dispatcher.KafkaDispatcher;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class NewOrderMain {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        var orderDispatcher = new KafkaDispatcher<Order>();
        var emailDispatcher = new KafkaDispatcher<Email>();
        for (var i = 0; i < 5; i++) {
            var emailOrder = Math.random() + "@email.com";
            var orderId = UUID.randomUUID().toString();
            var amount = BigDecimal.valueOf(Math.random() * 5000 + 1);
            var order = new Order(orderId, amount, emailOrder);


            orderDispatcher.send(Constants.ECOMMERCE_NEW_ORDER, emailOrder, order, new CorrelationId(NewOrderMain.class.getSimpleName()));


        }
    }
}