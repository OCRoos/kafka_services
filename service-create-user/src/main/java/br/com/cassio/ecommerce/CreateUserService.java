package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.consumer.ConsumerService;
import br.com.cassio.ecommerce.consumer.ServiceRunner;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.sql.SQLException;
import java.util.UUID;

public class CreateUserService implements ConsumerService<Order> {
    private final LocalDatabase database;

    CreateUserService() throws SQLException {
        this.database = new LocalDatabase("users_database");
        this.database.createIfExists(
                "Create table Users ( " +
                        "uuid varchar(200) primary key, " +
                        "email varchar(200)) ");
    }


    public static void main(String[] args) {
        new ServiceRunner(CreateUserService::new).start(1);
    }

    @Override
    public String getConsumerGroup() {
        return CreateUserService.class.getSimpleName();
    }

    @Override
    public String getTopic() {
        return Constants.ECOMMERCE_NEW_ORDER;
    }

    public void parse(ConsumerRecord<String, Message<Order>> record) throws SQLException {
        System.out.println("-----------------------------------------");
        System.out.println("Processing new order, checking for new users!");
        System.out.println(record.value());
        var message = record.value();
        var order = message.getPayload();
        // just pretending to check for fraud. This way we can test both scenarios
        if (isNewUser(order.getEmail())) {
            insertNewUser(order.getEmail());
        }
    }

    private void insertNewUser(String email) throws SQLException {
        var uuid = UUID.randomUUID().toString();
        database.update("insert into Users (uuid, email) values (?, ?)", uuid, email);
        System.out.println("User " + uuid + " and " + email + " added!");
    }

    private Boolean isNewUser(String email) throws SQLException {
        var results = database.query("Select uuid from Users where email = ? limit 1", email);
        return !results.next();
    }

}
