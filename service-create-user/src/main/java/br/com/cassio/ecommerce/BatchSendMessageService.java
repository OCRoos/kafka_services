package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.consumer.KafkaService;
import br.com.cassio.ecommerce.dispatcher.KafkaDispatcher;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class BatchSendMessageService {
    private final Connection conn;

    BatchSendMessageService() throws SQLException {
        String url = "jdbc:sqlite:target/users_database.db";
        this.conn = DriverManager.getConnection(url);
    }

    public static void main(String[] args) throws SQLException {
        var service = new BatchSendMessageService();
        var kafkaService = new KafkaService<>(
                BatchSendMessageService.class.getSimpleName(),
                Constants.SEND_MESSAGE_TO_ALL_USERS,
                service::parse,
                new HashMap<>());
        kafkaService.run();
    }

    private static final KafkaDispatcher<User> userDispatcher = new KafkaDispatcher<>();

    private void parse(ConsumerRecord<String, Message<String>> record) throws SQLException {
        System.out.println("-----------------------------------------");
        System.out.println("Processing new order, checking for new users!");
        var message = record.value();
        System.out.println(message.getPayload());
        getAllUsers().forEach(user -> {
            System.out.println("Async " +user.getUuid());
            userDispatcher.sendAsync(message.getPayload(), user.getUuid(), user, message.getId().continueWith(BatchSendMessageService.class.getSimpleName()));
        });
    }

    private Collection<User> getAllUsers() throws SQLException {
        Collection<User> users = new ArrayList<>();
        var results = conn.prepareStatement("Select uuid from Users ").executeQuery();
        while (results.next()) {
            users.add(new User(results.getString(1)));
        }
        return users;
    }
}
