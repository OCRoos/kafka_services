package br.com.cassio.ecommerce;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class HttpEcommerceService {

    public static void main(String[] args) throws Exception {
        var server = new Server(8085);
        var context = new ServletContextHandler();
        context.setContextPath("/");
        context.addServlet(new ServletHolder(new NewOrderServelet()), "/new");
        context.addServlet(new ServletHolder(new GenerateAllUserReportServelet()), "/admin/reports/all-users");
        server.setHandler(context);
        server.start();
        server.join();
    }

//    public static void main(String[] args) throws ExecutionException, InterruptedException {

}
