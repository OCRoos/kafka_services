package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.dispatcher.KafkaDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class GenerateAllUserReportServelet extends HttpServlet {

    private final KafkaDispatcher<String> batchDispatcher = new KafkaDispatcher<>();

    @Override
    public void destroy() {
        super.destroy();
        try {
            batchDispatcher.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try {
            batchDispatcher.send(Constants.SEND_MESSAGE_TO_ALL_USERS,
                    Constants.USER_GENERATE_READING_REPORT,
                    Constants.USER_GENERATE_READING_REPORT,
                    new CorrelationId(GenerateAllUserReportServelet.class.getSimpleName()));
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().println("Report will be generated!");
            System.out.println("Report will be generated!, message sent to queue "+Constants.USER_GENERATE_ALL_READING_REPORT);
        } catch (ExecutionException e) {
            throw new ServletException(e);
        } catch (InterruptedException | IOException e) {
            throw new ServletException(e);
        }
    }
}
