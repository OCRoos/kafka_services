package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.dispatcher.KafkaDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class NewOrderServelet extends HttpServlet {

    private final KafkaDispatcher<Order> orderDispatcher = new KafkaDispatcher<>();
    private final KafkaDispatcher<Email> emailDispatcher = new KafkaDispatcher<>();

    @Override
    public void destroy() {
        super.destroy();
        try {
            orderDispatcher.close();
            emailDispatcher.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        var emailOrder = req.getParameter("email");
        var amount = new BigDecimal(req.getParameter("amount"));
//        var emailOrder = Math.random() + "@email.com";
        var orderId = UUID.randomUUID().toString();
//        var amount = BigDecimal.valueOf(Math.random() * 5000 + 1);
        var order = new Order(orderId, amount, emailOrder);
        var subject = "Thank you for buying with us!";
        var body = "Here is your order !";
        var email = new Email(subject, body);
        var className = NewOrderServelet.class.getSimpleName();
        try {
            orderDispatcher.send(Constants.ECOMMERCE_NEW_ORDER, emailOrder, order, new CorrelationId(className));
            emailDispatcher.send(Constants.ECOMMERCE_EMAIL, emailOrder, email, new CorrelationId(className));
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.getWriter().println("New order sent!");
            System.out.println("Your order has been processed");
        } catch (ExecutionException e) {
            throw new ServletException(e);
        } catch (InterruptedException | IOException e) {
            throw new ServletException(e);
        }
    }
}
