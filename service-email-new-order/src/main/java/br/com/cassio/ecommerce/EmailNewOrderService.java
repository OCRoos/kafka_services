package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.consumer.ConsumerService;
import br.com.cassio.ecommerce.consumer.ServiceRunner;
import br.com.cassio.ecommerce.dispatcher.KafkaDispatcher;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class EmailNewOrderService implements ConsumerService<Order> {

    public static void main(String[] args) throws IOException {
        new ServiceRunner(EmailNewOrderService::new).start(1);
    }

    private final KafkaDispatcher<Email> emailDispatcher = new KafkaDispatcher<>();

    @Override
    public void parse(ConsumerRecord<String, Message<Order>> record) {
        System.out.println("-----------------------------------------");
        System.out.println("Processing new order, preparing email for the new order!");
        System.out.println(record.key());
        System.out.println(record.value());
        System.out.println(record.offset());
        System.out.println(record.partition());
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        var message = record.value();
        var order = message.getPayload();
        // just pretending to check for fraud. This way we can test both scenarios
        var subject = "Thank you for buying with us!";
        var body = "Here is your order !";
        var email = new Email(subject, body);

        CorrelationId id = new CorrelationId(EmailNewOrderService.class.getSimpleName());
        try {
            emailDispatcher.send(Constants.ECOMMERCE_EMAIL, order.getEmail(), email, id.continueWith("_" + order.getEmail()));
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String getConsumerGroup() {
        return EmailNewOrderService.class.getSimpleName();
    }

    @Override
    public String getTopic() {
        return Constants.ECOMMERCE_EMAIL;
    }
}
