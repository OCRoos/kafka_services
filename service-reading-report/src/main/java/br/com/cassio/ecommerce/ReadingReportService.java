package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.consumer.ConsumerService;
import br.com.cassio.ecommerce.consumer.KafkaService;
import br.com.cassio.ecommerce.consumer.ServiceRunner;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;

public class ReadingReportService implements ConsumerService<User> {

    private static final Path SOURCE = new File("src/main/resources/report.txt").toPath();

    public static void main(String[] args) {
        new ServiceRunner(ReadingReportService::new).start(3);
    }

    @Override
    public String getConsumerGroup() {
        return ReadingReportService.class.getSimpleName();
    }

    @Override
    public String getTopic() {
        return Constants.USER_GENERATE_READING_REPORT;
    }

    public void parse(ConsumerRecord<String, Message<User>> record) {
        var message = record.value();
        var user = message.getPayload();

        System.out.println("-----------------------------------------");
        System.out.println("Creating report to user " + user.getUuid() + "!");
        var target = new File(user.getReportPath());
        try {
            IO.copy(SOURCE, target);
            IO.append(target, "Created for user: " + target.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
