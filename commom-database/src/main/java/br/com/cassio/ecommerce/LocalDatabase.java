package br.com.cassio.ecommerce;

import java.sql.*;

public class LocalDatabase {
    private final Connection conn;

    LocalDatabase(String name) throws SQLException {
        String url = "jdbc:sqlite:target/" + name + ".db";
        this.conn = DriverManager.getConnection(url);
        //CAREFUL. with these kind of try/catch it could be dangerous

    }

    public void createIfExists(String statement) {
        try {
            conn.createStatement().execute(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean update(String statement, String... args) throws SQLException {
        return prepare(statement, args).execute();
    }

    public ResultSet query(String statement, String... args) throws SQLException {
        return prepare(statement, args).executeQuery();
    }

    private PreparedStatement prepare(String statement, String[] args) throws SQLException {
        var preparedStatement = conn.prepareStatement(statement);
        for (int i = 0; i < args.length; i++) {
            preparedStatement.setString(i, args[i]);
        }
        return preparedStatement;
    }
}
