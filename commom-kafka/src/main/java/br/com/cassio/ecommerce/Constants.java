package br.com.cassio.ecommerce;

public class Constants {
    public static final String ECOMMERCE_EMAIL = "ECOMMERCE_EMAIL";
    public static final String ECOMMERCE_NEW_ORDER = "ECOMMERCE_NEW_ORDER";
    public static final String ECOMMERCE_NOT_FRAUD = "ECOMMERCE_NOT_FRAUD";
    public static final String ECOMMERCE_IS_FRAUD = "ECOMMERCE_IS_FRAUD";
    public static final String LOG_SERVICE = "ECOMMERCE.*";
    public static final String USER_GENERATE_READING_REPORT = "ECOMMERCE_USER_GENERATE_READING_REPORT";
    public static final String USER_GENERATE_ALL_READING_REPORT = "ECOMMERCE_USER_GENERATE_ALL_READING_REPORT";
    public static final String SEND_MESSAGE_TO_ALL_USERS = "ECOMMERCE_SEND_MESSAGE_TO_ALL_USERS";


    private Constants() {
    }

}