package br.com.cassio.ecommerce;

import com.google.gson.*;
import org.apache.kafka.common.serialization.Deserializer;

import java.lang.reflect.Type;

public class MessageAdapter implements JsonSerializer<Message>, JsonDeserializer<Message> {
    @Override
    public JsonElement serialize(Message message, Type type, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("type", message.getPayload().getClass().getName())  ;
        obj.add("correlationId", context.serialize(message.getId()));
        obj.add("payload", context.serialize(message.getPayload()));
        return obj;
    }


    @Override
    public Message deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        var obj = jsonElement.getAsJsonObject();
        var payloadType = obj.get("type").getAsString();
        var correlationId = (CorrelationId) context.deserialize(obj.get("correlationId"), CorrelationId.class);
        try {
            var payload = context.deserialize(obj.get("payload"), Class.forName(payloadType));
            return new Message(correlationId, payload);
            } catch (ClassNotFoundException e) {
            throw new JsonParseException(e);
        }
    }
}