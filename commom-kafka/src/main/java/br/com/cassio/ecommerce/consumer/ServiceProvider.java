package br.com.cassio.ecommerce.consumer;

import java.util.HashMap;
import java.util.concurrent.Callable;

public class ServiceProvider<T> implements Callable<Void> {

    private final ServiceFactory<T> factory;

    public ServiceProvider(ServiceFactory<T> factory) {
        this.factory = factory;
    }

    public Void call() throws Exception {
        var service = factory.create();
        try (var kafkaService = new KafkaService<>(
                service.getConsumerGroup(),
                service.getTopic(),
                service::parse,
                new HashMap<>())) {
            kafkaService.run();
        }
        return null;
    }
}
