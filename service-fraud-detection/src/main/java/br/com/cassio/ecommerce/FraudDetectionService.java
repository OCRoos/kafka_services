package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.consumer.ConsumerService;
import br.com.cassio.ecommerce.consumer.ServiceRunner;
import br.com.cassio.ecommerce.dispatcher.KafkaDispatcher;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

public class FraudDetectionService implements ConsumerService<Order> {

    private final LocalDatabase database;

    public static void main(String[] args) {
        new ServiceRunner(FraudDetectionService::new).start(5);
    }

    public FraudDetectionService() throws SQLException {
        this.database = new LocalDatabase("frauds_database");
        this.database.createIfExists(
                "Create table Orders ( " +
                        "uuid varchar(200) primary key, " +
                        "is_fraud boolean) ");
    }

    private Boolean wasProcessed(Order order) throws SQLException {
        return database.query("Select uuid from Users where uuid = ? limit 1", order.getOrderId()).next();
    }

    private final KafkaDispatcher<Order> kafkaDispatcher = new KafkaDispatcher<>();

    @Override
    public String getConsumerGroup() {
        return FraudDetectionService.class.getSimpleName();
    }

    @Override
    public String getTopic() {
        return Constants.ECOMMERCE_NEW_ORDER;
    }

    public void parse(ConsumerRecord<String, Message<Order>> record) throws SQLException {
        System.out.println("-----------------------------------------");
        System.out.println("Processing new order, checking for fraud!");
        System.out.println(record.key());
        System.out.println(record.value());
        System.out.println(record.offset());
        System.out.println(record.partition());
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        var message = record.value();
        var order = message.getPayload();
        // just pretending to check for fraud. This way we can test both scenarios
        if (wasProcessed(order)) {
            System.out.println("Order " + order.getOrderId() + " was already processed");
            return;
        }


        System.out.println(order);
        var className = FraudDetectionService.class.getSimpleName();
        if (isaFraud(order)) {
            database.update("insert into Users (uuid, is_fraud) values (?, true)", order.getOrderId());
            System.out.println("Is a fraud");
            try {
                kafkaDispatcher.send(Constants.ECOMMERCE_IS_FRAUD, order.getEmail(), order, message.getId().continueWith(className));
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            database.update("insert into Users (uuid, is_fraud) values (?, false)", order.getOrderId());
            System.out.println("Is not a fraud");
            try {
                kafkaDispatcher.send(Constants.ECOMMERCE_NOT_FRAUD, order.getEmail(), order, message.getId().continueWith(className));
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isaFraud(Order order) {
        return order.getAmount().compareTo(new BigDecimal("4500")) >= 0;
    }
}
