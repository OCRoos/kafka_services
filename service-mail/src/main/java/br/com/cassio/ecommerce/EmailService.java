package br.com.cassio.ecommerce;

import br.com.cassio.ecommerce.consumer.KafkaService;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.Map;

import static br.com.cassio.ecommerce.Constants.ECOMMERCE_EMAIL;

public class EmailService {

    public static void main(String[] args) {
        var emailService = new EmailService();
        var kafkaService = new KafkaService<>(
                EmailService.class.getSimpleName(),
                ECOMMERCE_EMAIL,
                emailService::parse,
                Map.of());
        kafkaService.run();
    }

    private void parse(ConsumerRecord<String, Message<Email>> record) {
        System.out.println("-----------------------------------------");
        System.out.println("Processing email!");
        System.out.println(record.key());
        System.out.println(record.value());
        System.out.println(record.offset());
        System.out.println(record.partition());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Email processed");
    }

}
